function [Querynames, ligandsNames, assignments, assignedAtomNames,  costs, haveQuery] = compareWithQuery( query, ligandsDir)
Querynames = extractfield(query.Model.HeterogenAtom, 'AtomName');
adeConnectivity = query.Connectivity;
adeNumAtoms = size(Querynames, 2);
adeBonds = zeros(adeNumAtoms);
for i=1:adeNumAtoms
    connect = adeConnectivity(i).BondAtomList;
    adeBonds(i, connect) = 1;
    adeBonds(connect, i) = 1;
end

X1 = extractfield(query.Model.HeterogenAtom, 'X').';
Y1 = extractfield(query.Model.HeterogenAtom, 'Y').';
ADEcoor = [X1 Y1].';

allLigandsDir = dir(ligandsDir);
ligandsNames = extractfield(allLigandsDir, 'name');
ligandsNames = ligandsNames(4:size(ligandsNames,2));

numLigands = size(ligandsNames, 2);
assignments = zeros(adeNumAtoms, numLigands);
assignedAtomNames = cell(adeNumAtoms, numLigands);
haveQuery = zeros(numLigands, 1);
costs = ones(numLigands, 1);

for i=1:numLigands
    currFilename = strcat(ligandsDir, ligandsNames{i});
    try
        mol2 = pdbread(currFilename);
        if (isempty(mol2))
            continue;
        end
        connectivity2 = mol2.Connectivity;
        names2 = extractfield(mol2.Model.HeterogenAtom, 'AtomName');
        numAtoms2 = size(names2, 2);
        bonds2 = zeros(numAtoms2);
        for j=1:numAtoms2
            if (size(connectivity2, 2)>=j)
                connect = connectivity2(j).BondAtomList;
                bonds2(j, connect) = 1;
                bonds2(connect, j) = 1;
            end
        end
        X2 = extractfield(mol2.Model.HeterogenAtom, 'X').';
        Y2 = extractfield(mol2.Model.HeterogenAtom, 'Y').';
        coor2 = [X2 Y2].';

        [ bestRot, bestTrans, cost, assignment ] = atoms_assignment( Querynames, ADEcoor, names2, coor2 );
        
        assignments(:, i) = assignment.';
        if(sum(assignments(:, i))>0)
            assignedAtomNames(:, i) = names2(assignment);
        end
        costs(i) = cost;
        if (cost<0.1)
            haveQuery(i) = 1;
        end

    catch
       warning('Problem with file %s', char(currFilename));
    end
end

end