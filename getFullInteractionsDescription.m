function [ allInteractions ] = getFullInteractionsDescription( pdbEntries_fields, dir,  interactionsPartialDescriptors)

allInteractions = struct;
for i=1:size(pdbEntries_fields, 1)
    curr_interaction = interactionsPartialDescriptors.(pdbEntries_fields{i});
    ligandAtomNames = fieldnames(curr_interaction);
    %currlig = char(ligandAtomNames(end, 1));
    currlig = curr_interaction.('currLig');
    currAtoms = [];
    curr_filename = pdbEntries_fields{i}(7:13);
    curr_filename = strcat(curr_filename, 'binding_atoms_', currlig, '.pdb');
    %curr_interaction = allInteractions.('field_3ikh_A_ATP');
    tmpfile = pdbread(strcat(dir, curr_filename));
    ligResSeq = curr_interaction.(currlig);
    ligResSeq = ligResSeq(1);
    pos=find(cell2mat({tmpfile.Model.HeterogenAtom(:).resSeq})==ligResSeq);
    
    ligandAtomNames = ligandAtomNames(1:2:end, :);
    
    for j=1:size(ligandAtomNames,1)
        if (~(strcmp(ligandAtomNames{j}, currlig)))
            posCurrAtom = find(strcmp({tmpfile.Model.HeterogenAtom(:).AtomName},strcat(ligandAtomNames{j}, 'A')) );
            if (isempty(posCurrAtom))
                posCurrAtom = find(strcmp({tmpfile.Model.HeterogenAtom(:).AtomName}, ligandAtomNames{j}));
            end
            posOfLigAtom = intersect(pos, posCurrAtom);
            
            X=[tmpfile.Model.HeterogenAtom(posOfLigAtom).X];
            Y=[tmpfile.Model.HeterogenAtom(posOfLigAtom).Y];
            Z=[tmpfile.Model.HeterogenAtom(posOfLigAtom).Z];
            coor = [X.' Y.' Z.'];
            curr_interaction.(strcat(ligandAtomNames{j}, '_coordinate')) = coor;
            
            interactingAtomNames = curr_interaction.(strcat(ligandAtomNames{j}, '_interactingAtomName'));
            interactingAtomNums = num2cell(curr_interaction.(ligandAtomNames{j})).';
            curr_interaction.(strcat(ligandAtomNames{j}, '_interactingCoordinate')) = [];
            curr_interaction.(strcat(ligandAtomNames{j}, '_interactingName')) = [];
            if (size(interactingAtomNums, 1)>0)
                
                for k=1:size(interactingAtomNums, 2)
                    posHetAtom=find(cell2mat({tmpfile.Model.HeterogenAtom(:).resSeq})==interactingAtomNums{k});
                    posCurrHetAtom = find(strcmp({tmpfile.Model.HeterogenAtom(:).AtomName},interactingAtomNames{k}) );
                    posCurr = intersect(posHetAtom, posCurrHetAtom);
                    if (~(isempty(posCurr)))
                        
                        X=[tmpfile.Model.HeterogenAtom(posCurr).X];
                        Y=[tmpfile.Model.HeterogenAtom(posCurr).Y];
                        Z=[tmpfile.Model.HeterogenAtom(posCurr).Z];
                        coor = [X.' Y.' Z.'];
                        interacionsRes = [tmpfile.Model.HeterogenAtom(posCurr).resName];
                        if (strcmp(interacionsRes, 'HOH'))
                            curr_interaction.(strcat(ligandAtomNames{j}, '_interactingName')) =  [curr_interaction.(strcat(ligandAtomNames{j}, '_interactingName')), {strcat(interacionsRes, '/', interactingAtomNames{k})}];
                            curr_interaction.(strcat(ligandAtomNames{j}, '_interactingCoordinate')) = [curr_interaction.(strcat(ligandAtomNames{j}, '_interactingCoordinate')); coor];
                        end
                    else
                        posAtom=find(cell2mat({tmpfile.Model.Atom(:).resSeq})==interactingAtomNums{k});
                        posCurrAtom = find(strcmp({tmpfile.Model.Atom(:).AtomName},interactingAtomNames{k}) );
                        posCurr = intersect(posAtom, posCurrAtom);
                        X=[tmpfile.Model.Atom(posCurr).X];
                        Y=[tmpfile.Model.Atom(posCurr).Y];
                        Z=[tmpfile.Model.Atom(posCurr).Z];
                        coor = [X.' Y.' Z.'];
                        interacionsRes = [tmpfile.Model.Atom(posCurr).resName];
                        curr_interaction.(strcat(ligandAtomNames{j}, '_interactingName')) =  [curr_interaction.(strcat(ligandAtomNames{j}, '_interactingName')), {strcat(aminolookup(interacionsRes), num2str(interactingAtomNums{k}), '/', interactingAtomNames{k})}];
                        curr_interaction.(strcat(ligandAtomNames{j}, '_interactingCoordinate')) = [curr_interaction.(strcat(ligandAtomNames{j}, '_interactingCoordinate')); coor];
                        
                    end
                    
                end
            end
        end
        
        
    end
    
    n6_interactions = curr_interaction.('N6_interactingCoordinate');
    n7Point = curr_interaction.('N7_coordinate');
    n1Point = curr_interaction.('N1_coordinate');
    n6interactingAtomNames = curr_interaction.('N6_interactingAtomName');
    n6interactingAtomNums = num2cell(curr_interaction.('N6')).';
    n6interactingNames = curr_interaction.('N6_interactingName');
    curr_interaction.('N6_WatsonCrick') = [];
    curr_interaction.('N6_WatsonCrick_interactingAtomName') = [];
    curr_interaction.('N6_WatsonCrick_interactingName') = [];
    curr_interaction.('N6_WatsonCrick_interactingCoordinate') = [];
    curr_interaction.('N6_Hoogsteen') = [];
    curr_interaction.('N6_Hoogsteen_interactingAtomName') = [];
    curr_interaction.('N6_Hoogsteen_interactingName') = [];
    curr_interaction.('N6_Hoogsteen_interactingCoordinate') = [];
    if (~(or (isempty(n7Point), isempty(n1Point))))
        for k=1:size(n6_interactions, 1)
            currPoint = n6_interactions(k, :);
            distn7 = pdist2(currPoint, n7Point, 'euclidean');
            distn1 = pdist2(currPoint, n1Point, 'euclidean');
            
            if (distn7>distn1)
                curr_interaction.('N6_WatsonCrick') = [curr_interaction.('N6_WatsonCrick'); n6interactingAtomNums{k}];
                curr_interaction.('N6_WatsonCrick_interactingAtomName') = [curr_interaction.('N6_WatsonCrick_interactingAtomName'); {n6interactingAtomNames{k}}];
                curr_interaction.('N6_WatsonCrick_interactingName') = [curr_interaction.('N6_WatsonCrick_interactingName'); {n6interactingNames{k}}];
                curr_interaction.('N6_WatsonCrick_interactingCoordinate') = [curr_interaction.('N6_WatsonCrick_interactingCoordinate'); n6_interactions(k, :)];
                
            else
                curr_interaction.('N6_Hoogsteen') = [curr_interaction.('N6_Hoogsteen'); n6interactingAtomNums{k}];
                curr_interaction.('N6_Hoogsteen_interactingAtomName') = [curr_interaction.('N6_Hoogsteen_interactingAtomName'); {n6interactingAtomNames{k}}];
                curr_interaction.('N6_Hoogsteen_interactingName') = [curr_interaction.('N6_Hoogsteen_interactingName'); {n6interactingNames{k}}];
                curr_interaction.('N6_Hoogsteen_interactingCoordinate') = [curr_interaction.('N6_Hoogsteen_interactingCoordinate'); n6_interactions(k, :)];
                
            end
        end
    else
        disp(strcat('problem with file:', pdbEntries_fields{i}));
    end
    
    allInteractions.(pdbEntries_fields{i}) = curr_interaction;
end

end
