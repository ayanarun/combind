==========================================
ComBind
==========================================

ComBind (Common Binding) is a Matlab-based pipeline to detect polar (hydrogen bonding) interactions of a given rigid ligand or a fragment of a ligand.
It is divided into two parts:
First, ComBind searches for all PDB ligands that contain the rigid fragment, using their planar representations. It downloads all the PDB files with these ligands, and aligns them according to the query fragment.
Then, the user should run Arpeggio to identify the polar interactions between the fragment and its interaction site, for each of the PDB files.
In the second part, ComBind uses Arpeggio's output to create a PyMOL session containing the fragment and the atoms of the interaction site.

==========================================
Prerequisites
==========================================
The first part of ComBind uses the Munkres algorithm to find the best assignment between the query fragment and each of the PDB ligands. An implementation of the algorithm can be downloaded from https://www.mathworks.com/matlabcentral/fileexchange/20652-hungarian-algorithm-for-linear-assignment-problems-v2-3
In order to generate the PyMOL sessions including the ligand and interaction site atoms, you will need to install:
Arpeggio: http://biosig.unimelb.edu.au/arpeggioweb/downloads/
PyMOL: http://pymol.org/dsc/

==========================================
Installing
==========================================
ComBind is a Matlab pipeline, which uses several local directories for its run.
The user will need to define the path of the following directories and files:

------------------------------------------
ComBind.m:
------------------------------------------
query_2d: the full path to the 2D PDB file of the query (can be obtained using openBabel)
query_2D_mirror: a path to the mirror structure of the query in 2D (can be obtained using openBabel)
ligand_2d_path: the full path to the directory of the PDB ligands in 2D representations
pdbLigandFile: a path to the PDB ligands file (can be downloaded from http://ligand-expo.rcsb.org/dictionaries/cc-to-pdb.tdd)
ade_3D: the full path to the ligand's 3D PDB file
path: the full path for the PDBs download directories.

------------------------------------------
runComBindAfterArpeggio.m:
------------------------------------------
pdbsDir: the full path to the aligned PDBs directory (can be the same as "path" in ConBind.m)
bindingSitesDir: the full path to where the binding sites will be saved (meaning - only the residues which hydrogen-bond to the query fragment)

==========================================
Running the tests
==========================================
Align a small number of adenine-containing ligands.
For this test, please download the zip file 2dLigands.zip from the Dowloads file, and the files: "example_cc-to-pdb.tdd", "ade_2D.pdb", "ade_2D_mirror.pdb", "ade_3D.pdb".
Then, set the parameters in ComBind.m:

query_2d: ADE_2D.pdb
query_2D_mirror: ADE_2D_mirror.pdb
ligand_2d_path: 2dLigands (full path)
pdbLigandFile: example_cc-to-pdb.tdd
ade_3D: ADE_3D.pdb
path: set a directory to download the PDB files.

now, run ComBind.m (by typing "ComBind.m" in the command window).

Once the run is finished (this sould take about two minutes), run Arpeggio on the results.
For this, you should open an extrenal shell, and type:
> cd path # path is the directory where ComBind downloaded the PDBs
> mkdir bindingSites
> for file in path/*.pdb; do { python /arpeggio/clean_pdb.py "$file" ; } ; done
> mkdir cleaned
> mv *.clean.pdb ./cleaned/
> cd cleaned
> ren '*.clean.pdb' '#1.pdb'
> mv * ../.
> cd ..
> for file in path/*.pdb; do {  python arpeggio.py "$file" -i 3.5 -co -0.1; } || { echo "Arpeggio had an error for file $file\n" >> path/errLog.txt; }; done
This will run Arpeggio on all the files, which could take a while.

Once the run is finished, you can run ComBind to generate the PyMOL files. Enter Matlab and set the parameters of runComBindAfterArpeggio.m:
pdbsDir: the same as "path" in ConBind.m
bindingSitesDir: path/bindingSites (where path is the full path to the PDBs directory)
Now, type runComBindAfterArpeggio.m. This should take a while.
At the end of the run, type in the command line: cmd and copy the results.
Open another external shell, and paste the value of cmd. This will open a PyMOL session with the adenine fragment and all the binding site atoms.
