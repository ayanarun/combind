% rigid fragment of interest file location
query_2d = pdbread('query_2D.pdb');
% rigid fragment of interest mirror file location
query_2D_mirror = pdbread('');
ligand_2d_path = '';

[queryAtomNames, ligandsNames, assignments, assignedAtomNames, costs, haveQuery] = compareWithQuery (query_2d, ligand_2d_path);
[~, ~, assignments_mirror, assignedAtomNames_mirror, costs_mirror, haveQuery_mirror] = compareWithQuery (query_2D_mirror, ligand_2d_path);
haveQuery = haveQuery+haveQuery_mirror;
assignments(:, haveQuery_mirror>0) = assignments_mirror(:, haveQuery_mirror>0);
% pdb ligands file: format: ligand name, list of lower case pdb ids seperated by a space: ATP 1atp ...
pdbLigandFile = 'cc-to-pdb.txt';
% files download path
path = '';
% query 3D file location
ligand_3D = pdbread('query_3D.pdb');
withDownload = 1;
withAlignments = 1;
[ligands, pdbEntries] = alignPDBsByLigand( pdbLigandFile, ligand_3D, path, ligand_2d_path, ligandsNames, haveQuery, assignments, withDownload, withAlignments);