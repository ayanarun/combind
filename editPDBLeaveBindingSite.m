function newPDB = editPDBLeaveBindingSite( pdbfile, LigandNum, chain, interactingSerNo, newFileName)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    % set the atom fields for the new protein
    atomNums = extractfield(pdbfile.Model.Atom, 'resSeq');
    hetatmNums = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
    
    [interactingResiduesNum, ~] = ismember(atomNums, interactingSerNo);
    [interactingHETATMNum, ~] = ismember(hetatmNums, interactingSerNo);
    
    interactingResiduesNum = atomNums(interactingResiduesNum);
    interactingHETATMNum = hetatmNums(interactingHETATMNum);
    
    newModel = extractfield(pdbfile, 'Model');
    newModel = newModel{:};
    chainID = extractfield(pdbfile.Model.Atom, 'chainID');
    rightChain = ismember(chainID, chain);
    [isBindingAtom, ~] = ismember(atomNums, interactingResiduesNum);
    isBindingAtom = and(isBindingAtom, rightChain);
    if (~isempty(find(isBindingAtom, 1)))
        atomSerNo = extractfield(pdbfile.Model.Atom, 'AtomSerNo');
        atomSerNo(~(isBindingAtom)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        atomName = extractfield(pdbfile.Model.Atom, 'AtomName');
        atomName(~(isBindingAtom)) = [];
        newAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newAtom.AtomSerNo] = atomSerNoCell{:};
        altLoc = extractfield(pdbfile.Model.Atom, 'altLoc');
        altLoc(~(isBindingAtom)) = [];
        [newAtom.altLoc] = altLoc{:};
        resName = extractfield(pdbfile.Model.Atom, 'resName');
        resName(~(isBindingAtom)) = [];
        [newAtom.resName] = resName{:};
        chainID = extractfield(pdbfile.Model.Atom, 'chainID');
        chainID(~(isBindingAtom)) = [];
        [newAtom.chainID] = chainID{:};
        resSeq = extractfield(pdbfile.Model.Atom, 'resSeq');
        resSeq(~(isBindingAtom)) = [];
        resSeqCell = num2cell(resSeq);
        [newAtom.resSeq] = resSeqCell{:};
        iCode = extractfield(pdbfile.Model.Atom, 'iCode');
        iCode(~(isBindingAtom)) = [];
        [newAtom.iCode] = iCode{:};
        X = extractfield(pdbfile.Model.Atom, 'X');
        X(~(isBindingAtom)) = [];
        XCell = num2cell(X);
        [newAtom.X] = XCell{:};
        Y = extractfield(pdbfile.Model.Atom, 'Y');
        Y(~(isBindingAtom)) = [];
        YCell = num2cell(Y);
        [newAtom.Y] = YCell{:};
        Z = extractfield(pdbfile.Model.Atom, 'Z');
        Z(~(isBindingAtom)) = [];
        ZCell = num2cell(Z);
        [newAtom.Z] = ZCell{:};
        occupancy = extractfield(pdbfile.Model.Atom, 'occupancy');
        occupancy(~(isBindingAtom)) = [];
        occupancyCell = num2cell(occupancy);
        [newAtom.occupancy] = occupancyCell{:};
        tempFactor = extractfield(pdbfile.Model.Atom, 'tempFactor');
        tempFactor(~(isBindingAtom)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newAtom.tempFactor] = tempFactorCell{:};
        segID = extractfield(pdbfile.Model.Atom, 'segID');
        segID(~(isBindingAtom)) = [];
        [newAtom.segID] = segID{:};
        element = extractfield(pdbfile.Model.Atom, 'element');
        element(~(isBindingAtom)) = [];
        [newAtom.element] = element{:};
        charge = extractfield(pdbfile.Model.Atom, 'charge');
        charge(~(isBindingAtom)) = [];
        [newAtom.charge] = charge{:};
        atomNameStruct = extractfield(pdbfile.Model.Atom, 'AtomNameStruct');
        atomNameStruct(~(isBindingAtom)) = [];
        atomNameStructCell = num2cell(atomNameStruct);
        [newAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpAtomNameStruct = [newAtom.AtomNameStruct];
        [newAtom.AtomNameStruct] = tmpAtomNameStruct{:};
        
        newModel = setfield(newModel, 'Atom', newAtom);

    else
        newModel = rmfield(newModel, 'Atom');
    end
    
    
    %create the het atom struct
    chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
    rightChain = ismember(chainID, chain);
    
    [isBindingHetAtom, ~] = ismember(hetatmNums, [interactingHETATMNum LigandNum]);
    isBindingHetAtom = and(isBindingHetAtom, rightChain);
    if (~isempty(find(isBindingHetAtom, 1)))
        atomSerNo = extractfield(pdbfile.Model.HeterogenAtom, 'AtomSerNo');
        atomSerNo(~(isBindingHetAtom)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        atomName = extractfield(pdbfile.Model.HeterogenAtom, 'AtomName');
        atomName(~(isBindingHetAtom)) = [];
        newHeterogenAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newHeterogenAtom.AtomSerNo] = atomSerNoCell{:};
        altLoc = extractfield(pdbfile.Model.HeterogenAtom, 'altLoc');
        altLoc(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.altLoc] = altLoc{:};
        resName = extractfield(pdbfile.Model.HeterogenAtom, 'resName');
        resName(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.resName] = resName{:};
        chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
        chainID(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.chainID] = chainID{:};
        resSeq = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
        resSeq(~(isBindingHetAtom)) = [];
        resSeqCell = num2cell(resSeq);
        [newHeterogenAtom.resSeq] = resSeqCell{:};
        iCode = extractfield(pdbfile.Model.HeterogenAtom, 'iCode');
        iCode(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.iCode] = iCode{:};
        X = extractfield(pdbfile.Model.HeterogenAtom, 'X');
        X(~(isBindingHetAtom)) = [];
        XCell = num2cell(X);
        [newHeterogenAtom.X] = XCell{:};
        Y = extractfield(pdbfile.Model.HeterogenAtom, 'Y');
        Y(~(isBindingHetAtom)) = [];
        YCell = num2cell(Y);
        [newHeterogenAtom.Y] = YCell{:};
        Z = extractfield(pdbfile.Model.HeterogenAtom, 'Z');
        Z(~(isBindingHetAtom)) = [];
        ZCell = num2cell(Z);
        [newHeterogenAtom.Z] = ZCell{:};
        occupancy = extractfield(pdbfile.Model.HeterogenAtom, 'occupancy');
        occupancy(~(isBindingHetAtom)) = [];
        occupancyCell = num2cell(occupancy);
        [newHeterogenAtom.occupancy] = occupancyCell{:};
        tempFactor = extractfield(pdbfile.Model.HeterogenAtom, 'tempFactor');
        tempFactor(~(isBindingHetAtom)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newHeterogenAtom.tempFactor] = tempFactorCell{:};
        segID = extractfield(pdbfile.Model.HeterogenAtom, 'segID');
        segID(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.segID] = segID{:};
        element = extractfield(pdbfile.Model.HeterogenAtom, 'element');
        element(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.element] = element{:};
        charge = extractfield(pdbfile.Model.HeterogenAtom, 'charge');
        charge(~(isBindingHetAtom)) = [];
        [newHeterogenAtom.charge] = charge{:};
        atomNameStruct = extractfield(pdbfile.Model.HeterogenAtom, 'AtomNameStruct');
        atomNameStruct(~(isBindingHetAtom)) = [];
        atomNameStructCell = num2cell(atomNameStruct);
        [newHeterogenAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpHetAtomNameStruct = [newHeterogenAtom.AtomNameStruct];
        [newHeterogenAtom.AtomNameStruct] = tmpHetAtomNameStruct{:};

        % create the new model struct
        newModel = setfield(newModel, 'HeterogenAtom', newHeterogenAtom);
    else
        newModel = rmfield(newModel, 'HeterogenAtom');
    end
    newPDB = pdbfile;
    newPDB = setfield(newPDB, 'Model', newModel);

   pdbwrite(newFileName, newPDB);
end

