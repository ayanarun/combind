function [R, t, rmsd] = myKabsch(pmatrix1, pmatrix2)
    % pmatrix - is supposed to be N*3 matrix, where N is the number of
    % atoms
    
    % get N - the number of atoms
    N = size(pmatrix1,1);
    
    %% step1: calculate the center of mass
    cmass1 = ones(N,1)*mean(pmatrix1);
    cmass2 = ones(N,1)*mean(pmatrix2);
    cpmatrix1 = pmatrix1-cmass1;
    cpmatrix2 = pmatrix2-cmass2;
    H = (cpmatrix1' * cpmatrix2);
    

    %% step2: calculate the SVD of the matrices and the translation
    [U,~,V] = svd(H);
    I = eye(3);
    R = V*I*U';
    if det(R) < 0
        %fprintf(1,'Reflection detected\n');
        I(3,3) = -1;
        R = V*I*U';
        %V(:,3) = -1*V(:,3);
        %R = V*U';
    end
    
    t = -R*cmass1' + cmass2';
    tpmatrix1 = R*pmatrix1' +t;
    tpmatrix1 = tpmatrix1';
    dist = tpmatrix1 - pmatrix2;
    dist = dist .* dist;
    dist = sum(dist(:));
    rmsd = sqrt(dist/N);
    
end