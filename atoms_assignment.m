function [ bestRot, bestTrans, minCost, bestAggisnment ] = atoms_assignment( atomNames1, coordinateSet1, atomNames2, coordinateSet2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    % get the locations of the nitrogen atoms - they will be used for
    % alignments, which will then be used for the assignments
    isO = strfind(atomNames1, 'N');
    locsO1 = find(~cellfun(@isempty,isO));
    isO = strfind(atomNames2, 'N');
    locsO2 = find(~cellfun(@isempty,isO));
    bestRot = zeros(2);
    bestTrans = zeros(2, 1);
    minCost = intmax;
    bestAggisnment = zeros(1,size(coordinateSet1,2));
    % get all the permutations of nitrogen atoms
    if ((size(locsO1,2)>=2) && (size(locsO2,2)>=2))
        permLocs1 = nchoosek(locsO1, 2);
        permLocs2 = nchoosek(locsO2, 2);

        % assignments
        angles = NaN(size(permLocs1, 1), size(permLocs2, 1));
        translations = zeros(1, 2, size(permLocs1, 1), size(permLocs2, 1));


        if ((size(permLocs1, 2)>1) && (size(permLocs2, 2)>1))

            % only the atoms - without their names. (e.g., 'N' instead of 'N6')
            atomsOnly1 = cellfun(@(x) x(1), atomNames1, 'UniformOutput', false) ;
            atomsOnly2 = cellfun(@(x) x(1), atomNames2, 'UniformOutput', false) ;

            % remove the coordinates of atoms found in one set and not found in the
            % other from the coordinates set
            for i=1:size(coordinateSet1, 2)
                checkStr = strfind(atomsOnly2, atomsOnly1(i));
                locs = find(~cellfun(@isempty, checkStr));
                if (isempty(locs)),
                    checkStr = strfind(atomsOnly1, atomsOnly1(i));
                    locs = find(~cellfun(@isempty, checkStr));
                    coordinateSet1(:, i) = NaN;
                end
            end
            for i=1:size(coordinateSet2, 2)
                checkStr = strfind(atomsOnly1, atomsOnly2(i));
                locs = find(~cellfun(@isempty, checkStr));
                if (isempty(locs)),
                    checkStr = strfind(atomsOnly2, atomsOnly2(i));
                    locs = find(~cellfun(@isempty, checkStr));
                    coordinateSet2(:, i) = NaN;
                end
            end

            for i=1:size(permLocs1, 1)
                v1 = [coordinateSet1(1, permLocs1(i, 2)), coordinateSet1(2, permLocs1(i, 2))] - [coordinateSet1(1, permLocs1(i, 1)), coordinateSet1(2, permLocs1(i, 1))];
                len_v1 = norm(v1);
                for j=1:size(permLocs2, 1)
                    v2 = [coordinateSet2(1, permLocs2(j, 2)), coordinateSet2(2, permLocs2(j, 2))] - [coordinateSet2(1, permLocs2(j, 1)), coordinateSet2(2, permLocs2(j, 1))];
                    len_v2 = norm(v2);
                    len_v1 = round(len_v1, 3);
                    len_v2 = round(len_v2, 3);
                    if (len_v1 == len_v2)
                        angle=acos( dot(v1,v2)/len_v1/len_v2 );
                        angles(i, j) = angle;

                        rotation = [cos(angle), -sin(angle); sin(angle), cos(angle)];
                        if (norm(rotation*v2' - v1') > 0.1)
                            angle = -angle;
                            angles(i, j) = angle;
                            rotation = [cos(angle), -sin(angle); sin(angle), cos(angle)];
                        end
                        coordinateSet2_rot = rotation*coordinateSet2;
                        distX = coordinateSet1(1, permLocs1(i, 2))-(coordinateSet2_rot(1, permLocs2(j, 2)));
                        distY = coordinateSet1(2, permLocs1(i, 2))-(coordinateSet2_rot(2, permLocs2(j, 2)));
                        translation = [distX, distY];
                        translations(:, :, i, j) = translation;
                        full_translation = repmat(translation, size(coordinateSet2_rot, 2), 1).';
                        new_coordinateSet2 = coordinateSet2_rot+full_translation;

                        % create the ditace matrix from each ligand atom to all ADE atoms
                        % in order to find the optimal assignment
                        D = pdist2(real(coordinateSet1.'), real(new_coordinateSet2.'));

                        % run munkers to find the assignment for pairs of the same
                        % atom type.
                        uniq_atomNames1 = unique(atomsOnly1);
                        completeAssignment = zeros(1,size(coordinateSet1,2));
                        completeCost = 0;
                        for k=1:size(uniq_atomNames1, 2)
                            is1 = strfind(atomNames1, uniq_atomNames1(k));
                            locs1 = find(~cellfun(@isempty,is1));
                            is2 = strfind(atomNames2, uniq_atomNames1(k));
                            locs2 = find(~cellfun(@isempty,is2));
                            coors1 = coordinateSet1(:, locs1);
                            coors2 = NaN(size(new_coordinateSet2, 1), size(new_coordinateSet2, 2));
                            coors2(:, locs2) = new_coordinateSet2(:, locs2);
                            D = pdist2(real(coors1.'), real(coors2.'));
                            [assignment,cost] = munkres(D);
                            completeAssignment(locs1) = assignment;
                            completeCost = completeCost+cost;
                        end

                        if (sum(completeAssignment>0)==size(completeAssignment, 2))
                            order_atomNames2 = atomNames2(completeAssignment);
                            atomsOnly2 = cellfun(@(x) x(1), order_atomNames2, 'UniformOutput', false) ;
                            atomsOnly1 = cellfun(@(x) x(1), atomNames1, 'UniformOutput', false) ;
                            if (strcmp(atomsOnly1, atomsOnly2))
                                if (completeCost<minCost)
                                    minCost = completeCost;
                                    bestAggisnment = completeAssignment;
                                    bestRot = rotation;
                                    bestTrans = translation;
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

