function newPDB = editPDBLeaveBindingAtoms( pdbfile, LigandNum, chain, interactingSerNo, interactingAtomName, newFileName)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    % set the atom fields for the new protein
    atomNums = extractfield(pdbfile.Model.Atom, 'resSeq');
    hetatmNums = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
    
    [interactingResiduesNum, ~] = ismember(atomNums, interactingSerNo);
    [interactingHETATMNum, ~] = ismember(hetatmNums, interactingSerNo);
    
    interactingResiduesNum = atomNums(interactingResiduesNum);
    interactingHETATMNum = hetatmNums(interactingHETATMNum);
    
    newModel = extractfield(pdbfile, 'Model');
    newModel = newModel{:};
    chainID = extractfield(pdbfile.Model.Atom, 'chainID');
    rightChain = ismember(chainID, chain);
    [isBindingAtom, ~] = ismember(atomNums, interactingResiduesNum);
    if (~isempty(find(isBindingAtom, 1)))
    %     atomNums(~(isBindingAtom)) = [];
    %     newAtom.resSeq = atomNums;
        atomSerNo = extractfield(pdbfile.Model.Atom, 'AtomSerNo');
        
        %[newPDB.Model.Atom.AtomSerNo] = atomSerNo;
        atomName = extractfield(pdbfile.Model.Atom, 'AtomName');
        
        for i=1:size(atomNums, 2)
            currAtomNo = atomNums(i);
            allZeros = zeros(1,size(isBindingAtom, 2));
            if sum(interactingSerNo==currAtomNo)>0
                containedAtoms = interactingAtomName(interactingSerNo==currAtomNo);
                for j=1:size(containedAtoms,2)
                    allZeros(strcmp(containedAtoms{j}, atomName))=1;        
                end
                changeIsBinding = or(~(currAtomNo==atomNums),allZeros);
                isBindingAtom(currAtomNo==atomNums) = changeIsBinding(currAtomNo==atomNums);
            end
        end
        isBindingAtom = and(isBindingAtom, rightChain);
        atomSerNo(~(isBindingAtom)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        atomName(~(isBindingAtom)) = [];
        
        newAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newAtom.AtomSerNo] = atomSerNoCell{:};
        %[newPDB.Model.Atom.AtomName] = atomName;
        altLoc = extractfield(pdbfile.Model.Atom, 'altLoc');
        altLoc(~(isBindingAtom)) = [];
    %     altLocCell = num2cell(altLoc.');
        [newAtom.altLoc] = altLoc{:};
        %[newPDB.Model.Atom.altLoc] = altLoc;
        resName = extractfield(pdbfile.Model.Atom, 'resName');
        resName(~(isBindingAtom)) = [];
    %     resNameCell = num2cell(resName);
        [newAtom.resName] = resName{:};
        %[newPDB.Model.Atom.resName] = resName;
        chainID = extractfield(pdbfile.Model.Atom, 'chainID');
        chainID(~(isBindingAtom)) = [];
    %     chainIDCell = num2cell(chainID);
        [newAtom.chainID] = chainID{:};
        %[newPDB.Model.Atom.chainID] = chainID;
        resSeq = extractfield(pdbfile.Model.Atom, 'resSeq');
        resSeq(~(isBindingAtom)) = [];
        resSeqCell = num2cell(resSeq);
        [newAtom.resSeq] = resSeqCell{:};
        %[newPDB.Model.Atom.resSeq] = resSeq;
        iCode = extractfield(pdbfile.Model.Atom, 'iCode');
        iCode(~(isBindingAtom)) = [];
        %iCodeCell = num2cell(iCode);
        [newAtom.iCode] = iCode{:};
        %[newPDB.Model.Atom.iCode] = iCode;
        X = extractfield(pdbfile.Model.Atom, 'X');
        X(~(isBindingAtom)) = [];
        XCell = num2cell(X);
        [newAtom.X] = XCell{:};
        %[newPDB.Model.Atom.X] = X;
        Y = extractfield(pdbfile.Model.Atom, 'Y');
        Y(~(isBindingAtom)) = [];
        YCell = num2cell(Y);
        [newAtom.Y] = YCell{:};
        %[newPDB.Model.Atom.Y] = Y;
        Z = extractfield(pdbfile.Model.Atom, 'Z');
        Z(~(isBindingAtom)) = [];
        ZCell = num2cell(Z);
        [newAtom.Z] = ZCell{:};
        %[newPDB.Model.Atom.Z] = Z;
        occupancy = extractfield(pdbfile.Model.Atom, 'occupancy');
        occupancy(~(isBindingAtom)) = [];
        occupancyCell = num2cell(occupancy);
        [newAtom.occupancy] = occupancyCell{:};
        %[newPDB.Model.Atom.occupancy] = occupancy;
        tempFactor = extractfield(pdbfile.Model.Atom, 'tempFactor');
        tempFactor(~(isBindingAtom)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newAtom.tempFactor] = tempFactorCell{:};
        %[newPDB.Model.Atom.tmpFactor] = tmpFactor;
        segID = extractfield(pdbfile.Model.Atom, 'segID');
        segID(~(isBindingAtom)) = [];
    %     segIDCell = num2cell(segID);
        [newAtom.segID] = segID{:};
        %[newPDB.Model.Atom.seqID] = seqID;
        element = extractfield(pdbfile.Model.Atom, 'element');
        element(~(isBindingAtom)) = [];
    %     elementCell = num2cell(element);
        [newAtom.element] = element{:};
        %[newPDB.Model.Atom.element] = element;
        charge = extractfield(pdbfile.Model.Atom, 'charge');
        charge(~(isBindingAtom)) = [];
    %     chargeCell = num2cell(charge);
        [newAtom.charge] = charge{:};
        %[newPDB.Model.Atom.charge] = charge;
        atomNameStruct = extractfield(pdbfile.Model.Atom, 'AtomNameStruct');
        atomNameStruct(~(isBindingAtom)) = [];
        atomNameStructCell = num2cell(atomNameStruct);
        [newAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpAtomNameStruct = [newAtom.AtomNameStruct];
        [newAtom.AtomNameStruct] = tmpAtomNameStruct{:};
        %[newPDB.Model.Atom.AtomNameStruct] = atomNameStruct;
        
        newModel = setfield(newModel, 'Atom', newAtom);

    else
        newModel = rmfield(newModel, 'Atom');
    end
    
    
    %create the het atom struct
    chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
    rightChain = ismember(chainID, chain);
    
    [isBindingHetAtom, ~] = ismember(hetatmNums, [interactingHETATMNum LigandNum]);
    isBindingHetAtom = and(isBindingHetAtom, rightChain);
    if (~isempty(find(isBindingHetAtom, 1)))
    %     atomNums(~(isBindingAtom)) = [];
    %     newAtom.resSeq = atomNums;
        atomSerNo = extractfield(pdbfile.Model.HeterogenAtom, 'AtomSerNo');
        atomName = extractfield(pdbfile.Model.HeterogenAtom, 'AtomName');       
        
        for i=1:size(hetatmNums, 2)
            currAtomNo = hetatmNums(i);
            allZeros = zeros(1,size(isBindingHetAtom, 2));
            if sum(interactingSerNo==currAtomNo)>0
                containedAtoms = interactingAtomName(interactingSerNo==currAtomNo);
                for j=1:size(containedAtoms,2)
                    allZeros(strcmp(containedAtoms{j}, atomName))=1;        
                end
                changeIsBinding = or(~(currAtomNo==hetatmNums),allZeros);
                isBindingHetAtom(currAtomNo==hetatmNums) = changeIsBinding(currAtomNo==hetatmNums);
            end
        end
        isBindingHetAtom = and(isBindingHetAtom, rightChain);
    

        atomSerNo(~(isBindingHetAtom)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        %[newPDB.Model.Atom.AtomSerNo] = atomSerNo;

        atomName(~(isBindingHetAtom)) = [];
        newHeterogenAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newHeterogenAtom.AtomSerNo] = atomSerNoCell{:};
        %[newPDB.Model.Atom.AtomName] = atomName;
        altLoc = extractfield(pdbfile.Model.HeterogenAtom, 'altLoc');
        altLoc(~(isBindingHetAtom)) = [];
        %altLocCell = num2cell(altLoc);
        [newHeterogenAtom.altLoc] = altLoc{:};
        %[newPDB.Model.Atom.altLoc] = altLoc;
        resName = extractfield(pdbfile.Model.HeterogenAtom, 'resName');
        resName(~(isBindingHetAtom)) = [];
    %     resNameCell = num2cell(resName);
        [newHeterogenAtom.resName] = resName{:};
        %[newPDB.Model.Atom.resName] = resName;
        chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
        chainID(~(isBindingHetAtom)) = [];
    %     chainIDCell = num2cell(chainID);
        [newHeterogenAtom.chainID] = chainID{:};
        %[newPDB.Model.Atom.chainID] = chainID;
        resSeq = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
        resSeq(~(isBindingHetAtom)) = [];
        resSeqCell = num2cell(resSeq);
        [newHeterogenAtom.resSeq] = resSeqCell{:};
        %[newPDB.Model.Atom.resSeq] = resSeq;
        iCode = extractfield(pdbfile.Model.HeterogenAtom, 'iCode');
        iCode(~(isBindingHetAtom)) = [];
        %iCodeCell = num2cell(iCode);
        [newHeterogenAtom.iCode] = iCode{:};
        %[newPDB.Model.Atom.iCode] = iCode;
        X = extractfield(pdbfile.Model.HeterogenAtom, 'X');
        X(~(isBindingHetAtom)) = [];
        XCell = num2cell(X);
        [newHeterogenAtom.X] = XCell{:};
        %[newPDB.Model.Atom.X] = X;
        Y = extractfield(pdbfile.Model.HeterogenAtom, 'Y');
        Y(~(isBindingHetAtom)) = [];
        YCell = num2cell(Y);
        [newHeterogenAtom.Y] = YCell{:};
        %[newPDB.Model.Atom.Y] = Y;
        Z = extractfield(pdbfile.Model.HeterogenAtom, 'Z');
        Z(~(isBindingHetAtom)) = [];
        ZCell = num2cell(Z);
        [newHeterogenAtom.Z] = ZCell{:};
        %[newPDB.Model.Atom.Z] = Z;
        occupancy = extractfield(pdbfile.Model.HeterogenAtom, 'occupancy');
        occupancy(~(isBindingHetAtom)) = [];
        occupancyCell = num2cell(occupancy);
        [newHeterogenAtom.occupancy] = occupancyCell{:};
        %[newPDB.Model.Atom.occupancy] = occupancy;
        tempFactor = extractfield(pdbfile.Model.HeterogenAtom, 'tempFactor');
        tempFactor(~(isBindingHetAtom)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newHeterogenAtom.tempFactor] = tempFactorCell{:};
        %[newPDB.Model.Atom.tmpFactor] = tmpFactor;
        segID = extractfield(pdbfile.Model.HeterogenAtom, 'segID');
        segID(~(isBindingHetAtom)) = [];
    %     segIDCell = num2cell(segID);
        [newHeterogenAtom.segID] = segID{:};
        %[newPDB.Model.Atom.seqID] = seqID;
        element = extractfield(pdbfile.Model.HeterogenAtom, 'element');
        element(~(isBindingHetAtom)) = [];
    %     elementCell = num2cell(element);
        [newHeterogenAtom.element] = element{:};
        %[newPDB.Model.Atom.element] = element;
        charge = extractfield(pdbfile.Model.HeterogenAtom, 'charge');
        charge(~(isBindingHetAtom)) = [];
    %     chargeCell = num2cell(charge);
        [newHeterogenAtom.charge] = charge{:};
        %[newPDB.Model.Atom.charge] = charge;
        atomNameStruct = extractfield(pdbfile.Model.HeterogenAtom, 'AtomNameStruct');
        atomNameStruct(~(isBindingHetAtom)) = [];
        %[newPDB.Model.Atom.AtomNameStruct] = atomNameStruct;
        atomNameStructCell = num2cell(atomNameStruct);
        [newHeterogenAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpHetAtomNameStruct = [newHeterogenAtom.AtomNameStruct];
        [newHeterogenAtom.AtomNameStruct] = tmpHetAtomNameStruct{:};

        % create the new model struct
        newModel = setfield(newModel, 'HeterogenAtom', newHeterogenAtom);
    else
        newModel = rmfield(newModel, 'HeterogenAtom');
    end
    newPDB = pdbfile;
    newPDB = setfield(newPDB, 'Model', newModel);
    
   try
        newFileName
        pdbwrite(newFileName, newPDB);
   catch ME
       errmsg = strcat('error writing file ', newFileName);
       disp (errmsg);
   end

end

