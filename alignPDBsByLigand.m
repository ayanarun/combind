function [ligands, pdbs] = alignPDBsByLigand( pdbLigandFile, ade, path, ligand_2d_path, ligandsNames, haveQuery, assignments, withDownload, withAlignments)

pdbURL = 'http://files.rcsb.org/view/';
fid = fopen(pdbLigandFile, 'r');
list = textscan(fid, '%s', 'Delimiter', '\n', 'whitespace', '');
list = list{1, 1};
fclose(fid);
X_3d_ade = extractfield(ade.Model.HeterogenAtom, 'X').';
Y_3d_ade = extractfield(ade.Model.HeterogenAtom, 'Y').';
Z_3d_ade = extractfield(ade.Model.HeterogenAtom, 'Z').';

ade_3d_coor = [X_3d_ade Y_3d_ade Z_3d_ade].';

ligands = cellfun(@(x) x(1:3), list, 'UniformOutput', false);
pdbs = cellfun(@(x) x(4:end), list, 'UniformOutput', false);
pdbs = cellfun(@(x) strsplit(x), pdbs, 'UniformOutput', false);

pdbs = cellfun(@(x) x(2:end), pdbs, 'UniformOutput', false);
for ligandsCounter=1:size(ligands, 1)
    currLigand = ligands{ligandsCounter};
    pdbsLigand = "";
    checkLig = find(contains(ligandsNames, currLigand));
    if (~(isempty(checkLig)))
        if (haveQuery(checkLig)>0)
            for pdbsCounter=1:size(pdbs{ligandsCounter}, 2)
                %download pdbs and align to ligand molecule
                currPDB = char(pdbs{ligandsCounter}(pdbsCounter));
                chainpdb = currPDB(end);
                currPDB = currPDB(1:end-2)
                if (withAlignments>0)
                    currFilename = strcat(path, '/', currPDB, '.pdb');
                    down = 0;
                    count = 0;
                    while((~(down)) && (count<10))
                        try
                            outfile = currFilename;
                            if (withDownload>0)
                                outfile = websave(currFilename, strcat(pdbURL, currPDB, '.pdb'));
                            end
                            down = 1;
                        catch ME
                            errmsg = strcat('try again to upload current PDB: ', currPDB);
                            disp (errmsg);
                            count = count+1;
                        end
                    end
                    if (count >=10)
                        errmsg = strcat('try again to upload current PDB: ', currPDB);
                        disp (errmsg);
                        continue;
                    end
                    try
                        pdb = pdbread(outfile);
                    catch ME
                        errmsg = strcat('problem with file ', outfile);
                        disp (errmsg);
                        continue;
                    end
                    if (isfield(pdb.Model, 'MDLSerNo'))
                        leaveFirstNMRModel(outfile);
                        pdb = pdbread(outfile);
                    end
                    chains = extractfield(pdb.Model.HeterogenAtom, 'chainID');
                    uchains = unique(chains);
                    ligand_2d = pdbread(strcat(ligand_2d_path, currLigand, '.pdb'));
                    ligand_2dNames = extractfield(ligand_2d.Model.HeterogenAtom, 'AtomName');
                    
                    for chainid=1:size(uchains, 2)
                        currChain = uchains(chainid);
                        if (strcmp(currChain, chainpdb))
                            chainPDB = leaveOnlyRelevantChain(pdb, currChain);
                            resNames = extractfield(chainPDB.Model.HeterogenAtom, 'resName');
                            ligandLocs = find(contains(resNames, currLigand));
                            if isempty(ligandLocs)
                                continue;
                            end
                            resChains = extractfield(chainPDB.Model.HeterogenAtom, 'chainID');
                            resChains = resChains(ligandLocs);
                            
                            
                            atomsNames = extractfield(chainPDB.Model.HeterogenAtom, 'AtomName');
                            
                            X_3d_ligand = extractfield(chainPDB.Model.HeterogenAtom, 'X').';
                            Y_3d_ligand = extractfield(chainPDB.Model.HeterogenAtom, 'Y').';
                            Z_3d_ligand = extractfield(chainPDB.Model.HeterogenAtom, 'Z').';
                            
                            atomsNames = atomsNames(ligandLocs);
                            X_3d_ligand = X_3d_ligand(ligandLocs);
                            Y_3d_ligand = Y_3d_ligand(ligandLocs);
                            Z_3d_ligand = Z_3d_ligand(ligandLocs);
                            
                            
                            resNums = extractfield(chainPDB.Model.HeterogenAtom, 'resSeq');
                            resNums = resNums(ligandLocs);
                            unums = unique(resNums);
                            unums1 = find(resNums == unums(1));
                            resChains = resChains(unums1);
                            resNums = resNums(unums1);
                            atomsNames = atomsNames(unums1);
                            X_3d_ligand = X_3d_ligand(unums1);
                            Y_3d_ligand = Y_3d_ligand(unums1);
                            Z_3d_ligand = Z_3d_ligand(unums1);
                            nonH = find(~(startsWith(atomsNames, 'H')));
                            resChains = resChains(nonH);
                            resNums = resNums(nonH);
                            atomsNames = atomsNames(nonH);
                            X_3d_ligand = X_3d_ligand(nonH);
                            Y_3d_ligand = Y_3d_ligand(nonH);
                            Z_3d_ligand = Z_3d_ligand(nonH);
                            
                            ligandAssignment = zeros(size(X_3d_ligand, 1), 1);
                            if (size(atomsNames, 2)==size(ligand_2dNames, 2))
                                for i=1:size(ligandAssignment, 1)
                                    curr = strcmp(atomsNames, ligand_2dNames{i});
                                    ligandAssignment(i) = find(curr);
                                end
                                ligand_3d_coor = [X_3d_ligand Y_3d_ligand Z_3d_ligand].';
                                ligand_names_reordered = atomsNames(:, ligandAssignment);
                                ligand_coor_reoreded = ligand_3d_coor(:, ligandAssignment);
                                [R, t, rmsd] = myKabsch(ligand_coor_reoreded(:, assignments(:, checkLig)).', ade_3d_coor.');
                                
                                %set the full atomic coordinates of the rotated file
                                full_X_3d_ligand = extractfield(chainPDB.Model.Atom, 'X').';
                                full_Y_3d_ligand = extractfield(chainPDB.Model.Atom, 'Y').';
                                full_Z_3d_ligand = extractfield(chainPDB.Model.Atom, 'Z').';
                                full_mol2_coors = [full_X_3d_ligand full_Y_3d_ligand full_Z_3d_ligand];
                                
                                % set the new coordinates
                                new_full_mol2_coors = R*full_mol2_coors.'+repmat(t(:, 1).', size(full_mol2_coors, 1), 1).';
                                ccoors = num2cell(round(new_full_mol2_coors(1, :).', 3));
                                [chainPDB.Model.Atom.X] = ccoors{:};
                                ccoors = num2cell(round(new_full_mol2_coors(2, :).', 3));
                                [chainPDB.Model.Atom.Y] = ccoors{:};
                                ccoors = num2cell(round(new_full_mol2_coors(3, :).', 3));
                                [chainPDB.Model.Atom.Z] = ccoors{:};
                                
                                
                                %also set the hetatom values
                                full_Xhet_3d_mol2 = extractfield(chainPDB.Model.HeterogenAtom, 'X').';
                                full_Yhet_3d_mol2 = extractfield(chainPDB.Model.HeterogenAtom, 'Y').';
                                full_Zhet_3d_mol2 = extractfield(chainPDB.Model.HeterogenAtom, 'Z').';
                                full_het_3d_mol2 = [full_Xhet_3d_mol2 full_Yhet_3d_mol2 full_Zhet_3d_mol2];
                                new_hetatoms_mol2_coors = R*full_het_3d_mol2.'+repmat(t(:, 1).', size(full_het_3d_mol2, 1), 1).';
                                ccoors = num2cell(new_hetatoms_mol2_coors(1, :).');
                                [chainPDB.Model.HeterogenAtom.X] = ccoors{:};
                                ccoors = num2cell(new_hetatoms_mol2_coors(2, :).');
                                [chainPDB.Model.HeterogenAtom.Y] = ccoors{:};
                                ccoors = num2cell(new_hetatoms_mol2_coors(3, :).');
                                [chainPDB.Model.HeterogenAtom.Z] = ccoors{:};
                                
                                % write output pdb
                                newFilename = strcat(currPDB, '_', currChain, '_', currLigand, '.pdb');
                                fullFilename = strcat(path, newFilename);
                                try
                                    pdbwrite(fullFilename{:}, chainPDB);
                                catch ME
                                    errmsg = strcat('error writing file ', newFilename);
                                    disp (errmsg);
                                end
                            else
                                errmsg = strcat('not enough ligand atoms: ', currLigand, 'in file: ', currPDB);
                                disp (errmsg);
                            end
                        end
                    end
                    if (withDownload>0)
                        delete(currFilename);
                    end
                end
            end
        end
    end
end
end