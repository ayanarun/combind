function [checkedPDBs, bins, sums2, numOfInteractionsForAtoms, allInteractions, commandline] = parseArpeggioForADE( pdbLigandFile, pdbsDir, arpgDir, bindingSitesDir, ligandsNames, ligsList, pdbEntries, ligandAtoms, assignedAtomNames, haveADE, withWriteFiles)
arpeggioExt = '.contacts';
pdbExt = '.pdb';

fid = fopen(pdbLigandFile, 'r');
list = textscan(fid, '%s', 'Delimiter', '\n', 'whitespace', '');
list = list{1, 1};
fclose(fid);
allInteractions = struct;
commandline = "open -a PyMol";

ligands = cellfun(@(x) x(1:3), list, 'UniformOutput', false);
pdbs = cellfun(@(x) x(4:end), list, 'UniformOutput', false);
pdbs = cellfun(@(x) x(:, 2:end), pdbs, 'UniformOutput', false);
pdbs = cellfun(@(x) strsplit(x), pdbs, 'UniformOutput', false);
numpdbs = cell2mat(cellfun(@(x) size(x, 2), pdbs, 'UniformOutput', false));
fullpdbs = cellfun(@(x) strjoin(x, ','), pdbs, 'UniformOutput', false);
fullpdbs = strjoin(fullpdbs, ',');
fullpdbs = strsplit(fullpdbs, ',');
errors = zeros(sum(numpdbs), 1);
bins = zeros(sum(numpdbs), size(ligandAtoms, 2));

for ligandsCounter=1:size(ligands, 1)
    currLigand = ligands{ligandsCounter};
    checkLig = find(contains(ligandsNames, currLigand));
    if (~(isempty(checkLig)))
        if (haveADE(checkLig)>0)
            for pdbsCounter=1:size(pdbs{ligandsCounter}, 2)
                chains = cellfun(@(x) x(end), pdbs{ligandsCounter}, 'UniformOutput', false);
                entry = lower(char(pdbs{ligandsCounter}(pdbsCounter)));
                entry = entry(1:4);
                chain = upper(chains{pdbsCounter});
                currPDB = strcat(pdbsDir, entry, '_', chain, '_', currLigand, pdbExt);
                currArpg = strcat(arpgDir, entry, '_', chain, '_', currLigand, arpeggioExt);
               try
                    pdbfile = pdbread(currPDB);
                    interactions = parseArpeggio(currArpg, pdbfile, currLigand, ligandAtoms(:, checkLig), assignedAtomNames(:, ligandsCounter), chain);
               catch
                    warning('Prolem with file %s', char(currArpg));
                    errorLoc = pdbsCounter;
                    if (ligandsCounter>1)
                        errorLoc = sum(numpdbs(1:ligandsCounter-1))+pdbsCounter;
                    end
                    errors(errorLoc) = 1;
                    continue;
                end

                allInteractionSerNo = [];
                allInteractionAtoms = [];
                for j=1:size(ligandAtoms, 1)
%                     bins(ligandsCounter, j) = size(interactions.(ligandAtoms{j, checkLig}), 1);
                    if (~isempty(interactions.(ligandAtoms{j, checkLig})))
                        if (size(interactions.(ligandAtoms{j, checkLig}), 2)>1)
                            interactions.(ligandAtoms{j, checkLig}) = interactions.(ligandAtoms{j, checkLig}).';
                        end
                        allInteractionSerNo = [allInteractionSerNo interactions.(ligandAtoms{j, checkLig}).'];
                        if (size(interactions.(strcat((ligandAtoms{j, checkLig}), '_interactingAtomName')),2)>1)
                            interactions.(strcat((ligandAtoms{j, checkLig}), '_interactingAtomName')) = (interactions.(strcat((ligandAtoms{j, checkLig}), '_interactingAtomName'))).';
                        end
                        allInteractionAtoms = [allInteractionAtoms (interactions.(strcat((ligandAtoms{j, checkLig}), '_interactingAtomName'))).'];
                    end
                end
                ligSerNo = interactions.(currLigand);
                interactions.('currLig') = currLigand;
                bindingAtomsPDB = strcat(bindingSitesDir, entry, '_', chain, '_binding_atoms_', currLigand, pdbExt);
                if (withWriteFiles>0)
                    bindingPDB = strcat(bindingSitesDir, entry, '_', chain, '_binding_site_', currLigand, pdbExt);
                    editPDBLeaveBindingSite(pdbfile, ligSerNo, chain, allInteractionSerNo, bindingPDB);
                    bindingAtomsPDB = strcat(bindingSitesDir, entry, '_', chain, '_binding_atoms_', currLigand, pdbExt);
                    editPDBLeaveBindingAtoms(pdbfile, ligSerNo, chain, allInteractionSerNo, allInteractionAtoms, bindingAtomsPDB);
                end
                s = ' ';
                commandline  = [commandline s bindingAtomsPDB];
                allInteractions.(strcat('field_', entry, '_', chain, '_', currLigand)) = interactions;

            end
        end
    end
end
% PDBs for which Arpeggio's run failed
errors = logical(errors);
checkedPDBs = cellfun(@(x) lower(x(1:4)), fullpdbs, 'UniformOutput', false);
checkedPDBs = checkedPDBs(errors==0);
entriesChecked = fullpdbs(errors==0);
bins(errors, :) = [];
bins2 = bins.';
sums2 = sum(bins2>0);
histTotNumberOfInteractions = histogram(sums2);
numOfInteractionsForAtoms = sum(bins>0);