function newPDB = leaveOnlyRelevantChain( pdbfile, chain)

    % set the atom fields for the new protein
    newModel = extractfield(pdbfile, 'Model');
    newModel = newModel{:};
    chainID = extractfield(pdbfile.Model.Atom, 'chainID');
    rightChain = ismember(chainID, chain);
    if (~isempty(find(rightChain, 1)))
        atomSerNo = extractfield(pdbfile.Model.Atom, 'AtomSerNo');
        atomSerNo(~(rightChain)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        atomName = extractfield(pdbfile.Model.Atom, 'AtomName');
        atomName(~(rightChain)) = [];
        newAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newAtom.AtomSerNo] = atomSerNoCell{:};
        altLoc = extractfield(pdbfile.Model.Atom, 'altLoc');
        altLoc(~(rightChain)) = [];
        [newAtom.altLoc] = altLoc{:};
        resName = extractfield(pdbfile.Model.Atom, 'resName');
        resName(~(rightChain)) = [];
        [newAtom.resName] = resName{:};
        chainID = extractfield(pdbfile.Model.Atom, 'chainID');
        chainID(~(rightChain)) = [];
        [newAtom.chainID] = chainID{:};
        resSeq = extractfield(pdbfile.Model.Atom, 'resSeq');
        resSeq(~(rightChain)) = [];
        resSeqCell = num2cell(resSeq);
        [newAtom.resSeq] = resSeqCell{:};
        iCode = extractfield(pdbfile.Model.Atom, 'iCode');
        iCode(~(rightChain)) = [];
        [newAtom.iCode] = iCode{:};
        X = extractfield(pdbfile.Model.Atom, 'X');
        X(~(rightChain)) = [];
        XCell = num2cell(X);
        [newAtom.X] = XCell{:};
        Y = extractfield(pdbfile.Model.Atom, 'Y');
        Y(~(rightChain)) = [];
        YCell = num2cell(Y);
        [newAtom.Y] = YCell{:};
        Z = extractfield(pdbfile.Model.Atom, 'Z');
        Z(~(rightChain)) = [];
        ZCell = num2cell(Z);
        [newAtom.Z] = ZCell{:};
        occupancy = extractfield(pdbfile.Model.Atom, 'occupancy');
        occupancy(~(rightChain)) = [];
        occupancyCell = num2cell(occupancy);
        [newAtom.occupancy] = occupancyCell{:};
        tempFactor = extractfield(pdbfile.Model.Atom, 'tempFactor');
        tempFactor(~(rightChain)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newAtom.tempFactor] = tempFactorCell{:};
        segID = extractfield(pdbfile.Model.Atom, 'segID');
        segID(~(rightChain)) = [];
        [newAtom.segID] = segID{:};
        element = extractfield(pdbfile.Model.Atom, 'element');
        element(~(rightChain)) = [];
        [newAtom.element] = element{:};
        charge = extractfield(pdbfile.Model.Atom, 'charge');
        charge(~(rightChain)) = [];
        [newAtom.charge] = charge{:};
        atomNameStruct = extractfield(pdbfile.Model.Atom, 'AtomNameStruct');
        atomNameStruct(~(rightChain)) = [];
        atomNameStructCell = num2cell(atomNameStruct);
        [newAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpAtomNameStruct = [newAtom.AtomNameStruct];
        [newAtom.AtomNameStruct] = tmpAtomNameStruct{:};
        
        newModel = setfield(newModel, 'Atom', newAtom);

    else
        newModel = rmfield(newModel, 'Atom');
    end
    
    
    %create the het atom struct
    chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
    rightChain = ismember(chainID, chain);
    if (~isempty(find(rightChain, 1)))
        atomSerNo = extractfield(pdbfile.Model.HeterogenAtom, 'AtomSerNo');
        atomSerNo(~(rightChain)) = [];
        atomSerNoCell = num2cell(atomSerNo);
        atomName = extractfield(pdbfile.Model.HeterogenAtom, 'AtomName');
        atomName(~(rightChain)) = [];
        newHeterogenAtom = struct('AtomSerNo', [], 'AtomName', atomName, 'altLoc', [], 'resName', [], 'chainID', [], 'resSeq', [],'iCode', [], 'X', [], 'Y', [], 'Z', [], 'occupancy', [], 'tempFactor', [], 'segID', [], 'element', [], 'charge', [], 'AtomNameStruct', []);
        [newHeterogenAtom.AtomSerNo] = atomSerNoCell{:};
        altLoc = extractfield(pdbfile.Model.HeterogenAtom, 'altLoc');
        altLoc(~(rightChain)) = [];
        [newHeterogenAtom.altLoc] = altLoc{:};
        resName = extractfield(pdbfile.Model.HeterogenAtom, 'resName');
        resName(~(rightChain)) = [];
        [newHeterogenAtom.resName] = resName{:};
        chainID = extractfield(pdbfile.Model.HeterogenAtom, 'chainID');
        chainID(~(rightChain)) = [];
        [newHeterogenAtom.chainID] = chainID{:};
        resSeq = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
        resSeq(~(rightChain)) = [];
        resSeqCell = num2cell(resSeq);
        [newHeterogenAtom.resSeq] = resSeqCell{:};
        iCode = extractfield(pdbfile.Model.HeterogenAtom, 'iCode');
        iCode(~(rightChain)) = [];
        [newHeterogenAtom.iCode] = iCode{:};
        X = extractfield(pdbfile.Model.HeterogenAtom, 'X');
        X(~(rightChain)) = [];
        XCell = num2cell(X);
        [newHeterogenAtom.X] = XCell{:};
        Y = extractfield(pdbfile.Model.HeterogenAtom, 'Y');
        Y(~(rightChain)) = [];
        YCell = num2cell(Y);
        [newHeterogenAtom.Y] = YCell{:};
        Z = extractfield(pdbfile.Model.HeterogenAtom, 'Z');
        Z(~(rightChain)) = [];
        ZCell = num2cell(Z);
        [newHeterogenAtom.Z] = ZCell{:};
        occupancy = extractfield(pdbfile.Model.HeterogenAtom, 'occupancy');
        occupancy(~(rightChain)) = [];
        occupancyCell = num2cell(occupancy);
        [newHeterogenAtom.occupancy] = occupancyCell{:};
        tempFactor = extractfield(pdbfile.Model.HeterogenAtom, 'tempFactor');
        tempFactor(~(rightChain)) = [];
        tempFactorCell = num2cell(tempFactor);
        [newHeterogenAtom.tempFactor] = tempFactorCell{:};
        segID = extractfield(pdbfile.Model.HeterogenAtom, 'segID');
        segID(~(rightChain)) = [];
        [newHeterogenAtom.segID] = segID{:};
        element = extractfield(pdbfile.Model.HeterogenAtom, 'element');
        element(~(rightChain)) = [];
        [newHeterogenAtom.element] = element{:};
        charge = extractfield(pdbfile.Model.HeterogenAtom, 'charge');
        charge(~(rightChain)) = [];
        [newHeterogenAtom.charge] = charge{:};
        atomNameStruct = extractfield(pdbfile.Model.HeterogenAtom, 'AtomNameStruct');
        atomNameStruct(~(rightChain)) = [];
        atomNameStructCell = num2cell(atomNameStruct);
        [newHeterogenAtom.AtomNameStruct] = atomNameStructCell{:};
        tmpHetAtomNameStruct = [newHeterogenAtom.AtomNameStruct];
        [newHeterogenAtom.AtomNameStruct] = tmpHetAtomNameStruct{:};

        % create the new model struct
        newModel = setfield(newModel, 'HeterogenAtom', newHeterogenAtom);
    else
        newModel = rmfield(newModel, 'HeterogenAtom');
    end
    newPDB = pdbfile;
    newPDB = setfield(newPDB, 'Model', newModel);

end