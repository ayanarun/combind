pdbsDir = '';
bindingSitesDir = '';
myLig = ligands;
myLigLoc = zeros(size(ligands, 1), 1);
for i=1:size(myLigLoc, 1)
    myLigLoc(i) = find(strcmp(ligandsNames, strcat(myLig{i}, '.pdb')));
end
withWriteFiles = 1;
assignedAtomNames(find((~cellfun(@isempty, assignedAtomNames)))) = erase(assignedAtomNames(find((~cellfun(@isempty, assignedAtomNames)))), "'");
[checkedPDBs, bins, sums2, numOfInteractionsForAtoms, allInteractions, commandLine] = parseArpeggioForADE( pdbLigandFile, pdbsDir, pdbsDir, bindingSitesDir, ligandsNames, myLig, pdbEntries, assignedAtomNames, assignedAtomNames(:, myLigLoc), haveQuery, withWriteFiles);

cmd = strjoin(commandLine);