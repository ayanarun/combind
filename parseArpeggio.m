function interactions = parseArpeggio( arpgfile, pdbfile, ligname, ligandAtoms, assignedAtomNames, chain )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

hohName = 'HOH';

% parse the pdb file to get the resnum of the ligand and the water oxygens
names = extractfield(pdbfile.Model.HeterogenAtom, 'resName');
nums = extractfield(pdbfile.Model.HeterogenAtom, 'resSeq');
ligandLocs = contains(names, ligname);
ligandLocs = nums(ligandLocs);
ligandLocs = unique(ligandLocs);
hohLocs = contains(names, hohName);
hohLocs = nums(hohLocs);
hohLocs = unique(hohLocs);

% parse arpeggio output file
fid = fopen(arpgfile, 'r');
list = textscan(fid, '%s', 'Delimiter', '\n', 'whitespace', '');
list = list{1,1};
fclose(fid);
tabs = cellfun(@(x) strsplit(x), list, 'UniformOutput', false);
firstAtom = cellfun(@(x) x{1},tabs, 'UniformOutput', false);
firstAtom = cellfun(@(x) strsplit(x, '/'), firstAtom, 'UniformOutput', false);
secondAtom = cellfun(@(x) x{2},tabs, 'UniformOutput', false);
secondAtom = cellfun(@(x) strsplit(x, '/'), secondAtom, 'UniformOutput', false);

% get all the relevant interactions: interactions involving the ligand, and
% classified as polar. Divide these interactions to ligand-protein and
% ligand-water interactions.

% first, remove non-polar interactions.
% include only h-bond interactions
polar = cell2mat(cellfun(@(x) logical(str2double(x{8})), tabs, 'UniformOutput', false));


firstAtom = firstAtom(polar);
secondAtom = secondAtom(polar);

% leave only interactions with atoms of the same chain
firstAtomChain = cellfun(@(x) x(1), firstAtom, 'UniformOutput', false);
irrelevantChain = ~contains(string(firstAtomChain), chain);
firstAtom(irrelevantChain) = [];
secondAtom(irrelevantChain) = [];
secondAtomChain = cellfun(@(x) x(1), secondAtom, 'UniformOutput', false);
irrelevantChain = ~contains(string(secondAtomChain), chain);
firstAtom(irrelevantChain) = [];
secondAtom(irrelevantChain) = [];

secondAtomID = cellfun(@(x) x(3), firstAtom, 'UniformOutput', false);
firstAtomID = cellfun(@(x) x(3), secondAtom, 'UniformOutput', false);

firstAtomNum = str2double(cellfun(@(y) cell2mat(y), cellfun(@(x) x(2), firstAtom, 'UniformOutput', false), 'UniformOutput', false));
firstAtomName = cellfun(@(y) cell2mat(y), cellfun(@(x) x(3), firstAtom, 'UniformOutput', false), 'UniformOutput', false);
secondAtomNum = str2double(cellfun(@(y) cell2mat(y), cellfun(@(x) x(2), secondAtom, 'UniformOutput', false), 'UniformOutput', false));
secondAtomName = cellfun(@(y) cell2mat(y), cellfun(@(x) x(3), secondAtom, 'UniformOutput', false), 'UniformOutput', false);

ligandInteractions = or(firstAtomNum==ligandLocs, secondAtomNum==ligandLocs);
if size(ligandLocs, 2)>1,
    for i=1:size(ligandLocs, 2)
        ligandInteractions = or(firstAtomNum==ligandLocs(i), secondAtomNum==ligandLocs(i));
        if sum(ligandInteractions)>0
            ligandLocs = ligandLocs(i);
            break;
        end
    end
end

firstAtom = firstAtom(ligandInteractions);
secondAtom = secondAtom(ligandInteractions);
firstAtomID = firstAtomID(ligandInteractions);
secondAtomID = secondAtomID(ligandInteractions);
firstAtomNum = firstAtomNum(ligandInteractions);
secondAtomNum = secondAtomNum(ligandInteractions);
firstAtomName = firstAtomName(ligandInteractions);
secondAtomName = secondAtomName(ligandInteractions);

ligFirstLocs = logical(firstAtomNum==ligandLocs);
ligSecLocs = logical(secondAtomNum==ligandLocs);

%mark the interactions involving water molecules
waterInteractions = logical(sum(or(firstAtomNum==hohLocs, secondAtomNum==hohLocs).'));
firstWithWater = firstAtom(waterInteractions);
secondWithWater = secondAtom(waterInteractions);
firstWithWaterAtomID = firstAtomID(waterInteractions);
secondWithWaterAtomID = secondAtomID(waterInteractions);
firstWithWaterAtomNum = firstAtomNum(waterInteractions);
secondWithWaterAtomNum = secondAtomNum(waterInteractions);

firstLigandAtomId = firstAtomID(ligSecLocs);
secondLigandAtomId = secondAtomID(ligFirstLocs);

interactions = struct;
for i=1:size(ligandAtoms, 1)
    atomName = ligandAtoms{i};
    firstCurrAtomInteractionLocs = strcmp(string(firstAtomID), assignedAtomNames{i});
    interactingFirst = firstAtomNum(firstCurrAtomInteractionLocs);
    interactingFirstName = firstAtomName(firstCurrAtomInteractionLocs);
    secondCurrAtomInteractionLocs = strcmp(string(secondAtomID), assignedAtomNames{i});
    interactingSecond = secondAtomNum(secondCurrAtomInteractionLocs);
    interactingSecondName = secondAtomName(secondCurrAtomInteractionLocs);
    interactions.(atomName) = [interactingFirst.' interactingSecond.'];
    interactions.(strcat(atomName, '_interactingAtomName')) = [interactingFirstName.' interactingSecondName.'];
end
interactions.(ligname) = ligandLocs;

end